Package.describe({
    name: "bff:slingshot",
    summary: "Directly post files to cloud storage services, such as AWS-S3.",
    version: "1.1.0",
    git: "https://gitlab.com/bigfatfile/meteor-slingshot",
});

Package.onUse(function (api) {
    api.versionsFrom("METEOR@2.6.1");

    api.use(["check", "ecmascript"]);
    api.use(["underscore"], "server");
    api.use(["tracker", "reactive-var"], "client");

    api.addFiles(["lib/restrictions.js", "lib/validators.js"]);

    api.addFiles("lib/upload.js", "client");

    api.addFiles(
        ["lib/directive.js", "lib/storage-policy.js", "services/aws-s3.js", "services/google-cloud.js", "services/rackspace.js"],
        "server"
    );

    api.export("Slingshot");
});

Package.onTest(function (api) {
    // You almost definitely want to depend on the package itself,
    // this is what you are testing!
    api.use("bff:slingshot");

    // You should also include any packages you need to use in the test code
    api.use(["ecmascript", "random", "meteortesting:mocha"]);

    // Finally add an entry point for tests
    api.mainModule("slingshot-tests.js");
});
